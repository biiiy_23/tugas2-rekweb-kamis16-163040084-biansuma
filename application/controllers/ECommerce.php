<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ECommerce extends CI_Controller {

		public function index() {
			if ($this->session->has_userdata('username')) {
				if ($this->session->userdata('username') == 'Admin') {
					redirect('Admin/goToHalamanAdmin');
				} else {
					redirect('Admin/goToCustomerPage');
				}
			} else {
				$this->load->view('Login');
			}
		}

		public function goToCustomerPage() {
			if ($this->session->has_userdata('username')) {
				if ($this->session->userdata('username') != 'Admin') {
					$this->load->model('Obat_model');
					$data['obat'] = $this->Obat_model->getObat();
					$data['content'] = 'customer';
			
					$this->load->view('customer/template', $data);
				} else {
					redirect('ECommerce/goToHalamanAdmin');
				}
			} else {
				redirect('ECommerce');
			}
			
		}

		public function setToCart($id) {
			$this->load->model('Obat_model');
			$data = $this->Obat_model->getObatById($id);
			if ($data['stok'] == 0) {
				$this->session->set_flashdata('status_salah', 'Stok Habis!');
				redirect('ECommerce/goToCustomerPage');
			} else {
				$cart = [
					'id' => $data['id_obat'],
					'name' => $data['nama_obat'],
					'qty' => $data['stok'],
					'price' => $data['harga'],
					'jenis_obat' => $data['jenis_obat']
				];
			$status = $this->cart->insert($cart);
			redirect('ECommerce/cartPage');
			}
		}

		public function cartPage() {
			$this->load->model('Obat_model');
			$data['content'] = 'order';

			$this->load->view('customer/template', $data);
		}

		public function deleteItem($rowid) {
			$this->cart->update(['rowid' => $rowid, 'qty' => 0]);
			redirect('ECommerce/cartPage');
		}

		public function saveTransaction() {
			$this->load->model('Transaction_model');

			$data = [
				'tanggal_transaksi' => date("Y-m-d"),
				'id_user' => $this->session->userdata('id_user')
			];

			$transaksi = $this->Transaction_model->insertTransaction($data);
			$transaksi_id = $this->db->insert_id();

			foreach ($this->cart->contents() as $items) {
				$detail = [
					'id_transaksi' => $transaksi_id,
					'id_obat' => $items['id']
				];
				$status = $this->Transaction_model->insertDetailTransaction($detail);
				$this->Transaction_model->updateTransaction($detail['id_obat'], $items['qty']);
			}

			$this->cart->destroy();
			redirect('ECommerce/goToCustomerPage');
		}

		public function searchObat() {
			$this->load->model('Obat_model');
			$keyword = $this->input->get('keyword');
			$data['obat'] = $this->Obat_model->searchObat($keyword)->result();
			$data['content'] = 'customer';

			$this->load->view('customer/template', $data);
		}

		public function cekLogin() {
			$this->_userRules();
			$this->load->model('User_model');

			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('status', 'Masukkan username dan password!');
				redirect('ECommerce');
			} else {
				$user = [
					'username' => $username,
					'password' => $password
				];

				$status = $this->User_model->getUser($user);

				if ($status) {
					if ($status['user_level'] == 1) {
						$this->session->set_userdata($status);
						redirect('ECommerce/goToHalamanAdmin');
					} else {
						$this->session->set_userdata($status);
						redirect('ECommerce/goToCustomerPage');
					}
					
				} else {
					$this->session->set_flashdata('status', 'Username atau password salah!');
					redirect('ECommerce');
				}
			}
		}

		public function logout() {
			$this->session->sess_destroy();
			redirect('ECommerce');
		}

		public function _userRules() {
			$this->form_validation->set_rules('username', 'username', 'trim|required');
			$this->form_validation->set_rules('password', 'password', 'trim|required');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		}

		public function goToHalamanAdmin() {
			if ($this->session->has_userdata('username') == 'Admin') {
				$this->load->model('Obat_model');
				$data['obat'] = $this->Obat_model->getObat();
				$data['content'] = 'Home';
			
				$this->load->view('templates/template', $data);
			} else {
				redirect('ECommerce');
			}
		}

		public function goToAddPage() {
			if ($this->session->has_userdata('username')) {
				$data['content'] = 'insert';
				$this->load->view('templates/template', $data);
			} else {
				redirect('ECommerce');
			}
		}

		public function add() {
			$this->_obatRules();
			$this->load->model('Obat_model');

			$id = $this->input->post('id_obat');
			$nama_obat = $this->input->post('nama_obat');
			$jenis_obat = $this->input->post('jenis_obat');
			$stok = $this->input->post('stok');
			$harga = $this->input->post('harga');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('status_salah', 'Isikan semua data obat dengan benar!');
				redirect('ECommerce/goToHalamanAdmin');
			} else {
				$obat = [
					'id_obat' => $id,
					'nama_obat' => $nama_obat,
					'jenis_obat' => $jenis_obat,
					'stok' => $stok,
					'harga' => $harga
				];

				$status = $this->Obat_model->insertObat($obat);

				if ($status) {
					$this->session->set_flashdata('status', 'Obat berhasil ditambahkan');
				} else {
					$this->session->set_flashdata('status_salah', 'Obat gagal ditambahkan');
				}
				redirect('ECommerce/goToHalamanAdmin');
			}
		}

		public function _obatRules() {
			$this->form_validation->set_rules('id_obat', 'id_obat', 'trim|required');
			$this->form_validation->set_rules('nama_obat', 'nama_obat', 'trim|required');
			$this->form_validation->set_rules('jenis_obat', 'jenis_obat', 'trim|required');
			$this->form_validation->set_rules('stok', 'stok', 'trim|required');
			$this->form_validation->set_rules('harga', 'harga', 'trim|required');
			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		}

		public function delete($id) {
			$this->load->model('Obat_model');
			$status = $this->Obat_model->deleteObat($id);
			if ($status) {
				$this->session->set_flashdata('status', 'Obat berhasil di hapus!');
			}
			redirect('ECommerce/goToHalamanAdmin');
		}

		public function search() {
			$this->load->model('Obat_model');
			$keyword = $this->input->get('keyword');
			$data['obat'] = $this->Obat_model->searchObat($keyword)->result();
			$data['content'] = 'home';

			$this->load->view('templates/template', $data);
		}

		public function goToUpdatePage($id) {
			if ($this->session->has_userdata('username')) {
				$this->load->model('Obat_model');
				$data['dataObat'] = $this->Obat_model->getObatById($id);
				$data['content'] = 'update';
				$this->load->view('templates/template', $data);
			} else {
				redirect('ECommerce');
			}
		}

		public function update($id) {
			$this->_obatRules();
			$this->load->model('Obat_model');

			$id_obat = $this->input->post('id_obat');
			$nama_obat = $this->input->post('nama_obat');
			$jenis_obat = $this->input->post('jenis_obat');
			$stok = $this->input->post('stok');
			$harga = $this->input->post('harga');

			if ($this->form_validation->run() == FALSE) {
				$this->session->set_flashdata('status_salah', 'Inputan Salah');
				redirect('ECommerce/goToHalamanAdmin');
			} else {	
				$obat = [
					'id_obat' => $id_obat,
					'nama_obat' => $nama_obat,
					'jenis_obat' => $jenis_obat,
					'stok' => $stok,
					'harga' => $harga
				];

				$status = $this->Obat_model->UpdateObat($obat, $id);

				if ($status) {
					$this->session->set_flashdata('status', 'Obat berhasil diubah');
				} else {
					$this->session->set_flashdata('status_salah', 'Obat gagal diubah');
				}
				redirect('ECommerce/goToHalamanAdmin');
			}
		}

		public function Transaction() {
			if ($this->session->has_userdata('username')) {
				$this->load->model('Transaction_model');
				$data['transaksi'] = $this->Transaction_model->getTransaction();
				$data['content'] = 'transaction';
			
				$this->load->view('templates/template', $data);
			} else {
				redirect('ECommerce');
			}
		}

		public function detailTransaction($id) {
			if ($this->session->has_userdata('username')) {
				$this->load->model('Transaction_model');
				$data['detail'] = $this->Transaction_model->getIdDetailTransaction($id);
				$data['content'] = 'detail';
			
				$this->load->view('templates/template', $data);
			} else {
				redirect('ECommerce');
			}
		}
}