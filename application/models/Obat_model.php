<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat_model extends CI_Model {

	public function getObat(){
		$this->db->group_by('id_obat');
		return $this->db->get('obat')->result();
	}

	public function getObatById($id) {
		return $this->db->get_where('obat', ['id_obat' => $id])->row_array();
	}

	public function insertObat($obat){
		return $this->db->insert('obat', $obat);
	}	

	public function updateObat($obat, $id) {
		$this->db->where('id_obat', $id);
		$this->db->update('obat', $obat);
		return $this->db->affected_rows();
	}

	public function deleteObat($id) {
		$this->db->where('id_obat', $id);
		$this->db->delete('obat');
		return $this->db->affected_rows();
	}

	public function searchObat($keyword) {
		$this->db->select('*');
		$this->db->from('obat');
		$this->db->like('nama_obat', $keyword);
		return $this->db->get();
	}
}

?>
