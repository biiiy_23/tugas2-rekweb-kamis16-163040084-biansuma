<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model {
	public function getTransaction(){
		$this->db->select('*, user.username AS user');
		$this->db->from('transaksi');
		$this->db->join('user', 'transaksi.id_user = user.id_user');
		$this->db->group_by('id_transaksi');
		return $this->db->get()->result();
	}

	public function getDetailTransaction() {
		$this->db->select('detail_transaksi.id_detail AS id_detail, obat.nama_obat AS nama, obat.harga AS harga');
		$this->db->from('detail_transaksi');
		$this->db->join('obat', 'detail_transaksi.id_obat = obat.id_obat');
		$this->db->group_by('id_detail');
		return $this->db->get()->result();
	}

	public function getIdDetailTransaction($id) {
		$this->db->where('detail_transaksi.id_transaksi', $id);
		$this->db->select('detail_transaksi.id_detail AS id_detail, obat.nama_obat AS nama, obat.jenis_obat AS jenis, obat.harga AS harga');
		$this->db->from('detail_transaksi');
		$this->db->join('obat', 'detail_transaksi.id_obat = obat.id_obat');
		$this->db->join('transaksi', 'detail_transaksi.id_transaksi = transaksi.id_transaksi');
		$this->db->group_by('id_detail');
		return $this->db->get()->result();
	}

	public function insertTransaction($data) {
		$this->db->insert('transaksi', $data);
	}

	public function insertDetailTransaction($detail) {
		$this->db->insert('detail_transaksi', $detail);
	}

	public function updateTransaction($id, $qty) {
		$this->db->where('id_obat', $id);
		$this->db->update('obat', ['stok' => $qty-1]);
		return $this->db->affected_rows();
	}
}

?>