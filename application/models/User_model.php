<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function getUser($user) {
		return $this->db->get_where('user', ['username' => $user['username'], 'password' => $user['password']])->row_array();
	}

}

?>