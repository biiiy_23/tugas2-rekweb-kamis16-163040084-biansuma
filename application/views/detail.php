<br><div class="content-large-box">

    <?php $total = 0; ?>
    <?php foreach ($detail as $dt): ?>
        <div class="col-sm-3">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><b>Id Detail :</b> <?= $dt->id_detail; ?></h4>
                    <h4 class="card-title"><b>Nama Obat :</b> <?= $dt->nama ?></h4>
                    <h4 class="card-title"><b>Jenis Obat :</b> <?= $dt->jenis ?></h4>
                    <?php $total += $dt->harga; ?>
                </div>
            </div>
        </div>
    <?php endforeach ?>

    <h3 style="margin-left: 20px;">Total : Rp.<?= $total; ?></h3>
    

</div>