<br><div class="content-large-box">
    <?php if( $this->session->flashdata('status') ) : ?>
        <div class="alert alert-success" role="alert">
            <?= $this->session->flashdata('status'); ?>   
        </div>

    <?php elseif( $this->session->flashdata('status_salah') ) : ?>
        <div class="alert alert-danger" role="alert">
            <?= $this->session->flashdata('status_salah'); ?>   
        </div>
    <?php endif; ?>

    <div class="form-inline search-form" style="margin: 0 0 2% 5%">
        <form action="<?= site_url('ECommerce/search') ?>" method="GET" >
            <input type="text" class="keyword" name="keyword" placeholder="Cari...">
            <button type="submit" class="btn btn-default">Search</button>
        </form>
        <a href="<?= site_url('ECommerce/goToAddPage') ?>"><button class="btn btn-primary" style="margin-left: 400%">Tambah Obat</button></a>
    </div>

    <div class="table-stats ov-h table-light">
        <table class="table ">
            <thead>
                <tr>
                    <th>Id Obat</th>
                    <th>Nama Obat</th>
                    <th>Jenis Obat</th>
                    <th>Stok</th>
                    <th>Harga</th>
                    <th style="margin-left: 30%;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($obat)) : ?>
                <?php foreach($obat as $ob) : ?>
                <tr>
                    <td><?= $ob->id_obat ?></td>
                    <td><span class="name"><?= $ob->nama_obat ?></span></td>
                    <td><span class="product"><?= $ob->jenis_obat ?></span></td>
                    <td><span class="count"><?= $ob->stok ?></span></td>
                    <td><span class="count"><?= $ob->harga ?></span></td>
                    <td><?php if(!empty($ob)) : ?>
                        <a href="<?= site_url('ECommerce/goToUpdatePage/' . $ob->id_obat . '') ?>" class="btn btn-xs btn-success">Ubah</a>
                        <?php endif ?>
                        <a href="<?= site_url('ECommerce/delete/' . $ob->id_obat . '') ?>" class="btn btn-xs btn-danger">Hapus</a>
                    </td>
                </tr>
                    <?php endforeach ?>
                <?php else : ?>
                <tr>
                    <td colspan="7" style="text-align: center;">Data yang anda cari tidak ada</td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>