<br><div class="content-large-box">

    <div class="table-stats ov-h table-light">
        <table class="table ">
            <thead>
                <tr>
                    <th>Id Transaksi</th>
                    <th>Tanggal Transaksi</th>
                    <th>User</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($transaksi)) : ?>
                <?php foreach($transaksi as $ts) : ?>
                <tr>
                    <td><?= $ts->id_transaksi ?></td>
                    <td><span class="name"><?= $ts->tanggal_transaksi ?></span></td>
                    <td><span class="product"><?= $ts->user ?></span></td>
                    <td><?php if(!empty($ts)) : ?>
                        <a href="<?= site_url('ECommerce/detailTransaction/' . $ts->id_transaksi) ?>" class="btn btn-primary btn-xs">Detail</button>
                        <?php endif ?>
                    </td>
                </tr>
                    <?php endforeach ?>
                <?php else : ?>
                <tr>
                    <td colspan="7" style="text-align: center;">Data yang anda cari tidak ada</td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>