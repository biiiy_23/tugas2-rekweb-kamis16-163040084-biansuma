<h2 style="margin-left: 5%">Ubah Data Obat</h2><br>
<div class="form-inline" style="margin-left: 5%">
	<?= form_open_multipart('ECommerce/update/' . $dataObat['id_obat']); ?>
		<table class="table">
			<tr>
				<td>Id Obat</td>
				<td>:</td>
				<td><input type="text" id="id_obat" name="id_obat" value="<?= $dataObat['id_obat'] ?>"></td>
			</tr>
			<tr>
				<td>Nama Obat</td>
				<td>:</td>
				<td><input type="text" id="nama_obat" name="nama_obat" value="<?= $dataObat['nama_obat'] ?>"></td>
			</tr>
			<tr>
				<td>Jenis Obat</td>
				<td>:</td>
				<td><input type="text" id="jenis_obat" name="jenis_obat" value="<?= $dataObat['jenis_obat'] ?>"></td>
			</tr>
			<tr>
				<td>Stok</td>
				<td>:</td>
				<td><input type="number" id="stok" name="stok" min="1" max="500" value="<?= $dataObat['stok'] ?>"></td>
			</tr>
			<tr>
				<td>Harga</td>
				<td>:</td>
				<td><input type="number" id="harga" name="harga" value="<?= $dataObat['harga'] ?>"></td>
			</tr>
		</table>
		<button class="btn btn-primary" type="submit">Ubah Data</button>
		<a class="btn btn-xs btn-secondary" href="<?= site_url('ECommerce/goToHalamanAdmin') ?>">Kembali</a>
	</form>
</div>